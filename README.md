macosx-hashes
=============

This project provides a simple means to securely collect password hashes from 
a Mac OS X system and put them into a file encrypted for use by the Virginia
Tech IT Security Office (VT ITSO).

A Groovy script does the tricky work of obtaining and decoding password hashes 
from the Mac’s directory services, and puts them into a format suitable for use 
with the [hashcat] (http://hashcat.net) and [JTR] (http://www.openwall.com/john)
password recovery tools used by ITSO.

This has been tested on Mac OS X 10.11 (El Capitan) and Mac OS X 10.10 (Yosemite). 
Would appreciate feedback or issue reports for different versions of Mac OS X.


Prerequisites
-------------

In order to use this script you will need to install the following prerequisites:

* [GPGTools] (https://gpgtools.org)
* [Groovy] (http://www.groovy-lang.org/download.html) version 2.4.5 or later

The `groovy` shell and the `gpg2` command must be available on the path of the 
user who runs the script to retrieve hashes.

#### Installing groovy on Mac OS X

1. Download the binary distribution; it should be a file named `apache-groovy-binary-2.4.5.zip`.
2. Unzip the downloaded file to produce a folder named `groovy-2.4.5`.
3. Move the folder into place: `sudo mv groovy-2.4.5 /usr/local/groovy-2.4.5`).
4. Create a link to the current groovy version: `cd /usr/local; sudo ln -s groovy-2.4.5 groovy`.
5. Add `/usr/local/groovy/bin` to your PATH.


Installation
------------

Simply clone this repository into a convenient location.

1. Copy the clone URL that appears near the top of this page.  Use the SSH URL if you have an
   account on *git.it.vt.edu* that is setup with your SSH key.  Otherwise, use the HTTPS URL.
2. Open a Terminal.
3. Change to the desired directory location.
4. Clone the repository: `git clone {URL}`

The clone will be stored in the `macosx-hashes` subdirectory of the directory from which
you ran `git clone`.


Collecting Hashes
-----------------

1. Open a Terminal
2. Change to the directory containing the clone of this repository.
3. Run the script: `sudo ./retrieve-hashes.sh`

The resulting hashes will be written to an encrypted file stored as `/var/local/hashes.txt.asc`.
Only the IT security office can decrypt the file.

All intermediate artifacts created in temporary files on the local filesystem are securely
removed using `srm` before the script exits.


