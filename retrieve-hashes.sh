#!/bin/bash

type gpg2 > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "gpg2 not found" >&2
  exit 1
fi

type groovy > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "groovy not found" >&2
  exit 1
fi

INSTALL_BASE=$(dirname $0)
exec groovy ${INSTALL_BASE}/retrieveHashes.groovy
