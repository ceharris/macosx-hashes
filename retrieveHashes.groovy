#!/usr/bin/env groovy

import groovy.io.FileType;

import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermissions;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

def itsoKeyId = "7279C76A0FAC6413";
def gpg = null;
def plainTextFile = null;
def encryptedFile = new File("/var/tmp/hashes.txt.asc");
try {
  plainTextFile = File.createTempFile("hashes", ".txt");
  createHashes(plainTextFile);
  gpg = new Gpg();
  gpg.importKey(itsoPublicKey());
  gpg.encrypt(plainTextFile, encryptedFile, itsoKeyId);
  println("hashes successfully written to ${encryptedFile}");
}
finally {
  if (plainTextFile != null) {
    secureRemove(plainTextFile);    
  }
  if (gpg != null) {
    secureRemove(gpg.gpgHome);
  }
}

def createHashes(def file) {
  def writer = new PrintWriter(new FileWriter(file));
  def hashDir = new File("/var/db/dslocal/nodes/Default/users");
  hashDir.eachFile(FileType.FILES) {
    def hashData = extractHashData(it);
    if (hashData != null) {
      def username = parseUserName(it);
      def hashDocument = convertHashDataToXml(hashData);
      def attributes = parseHash(hashDocument);
      writeHash(username, attributes, writer);
    }
  }  
  writer.close();
  return file;
}

def parseHash(def document) {
  def nodeList = document.getElementsByTagName("key");
  for (int i = 0; i < nodeList.getLength(); i++) {
    def node = nodeList.item(i);
    if (node.textContent == "SALTED-SHA512-PBKDF2") {
      def attributes = [:];
      node = node.nextSibling.firstChild;
      def key = null;
      def value = null;
      while (node != null) {
        if (node.nodeType != Node.ELEMENT_NODE) {
          node = node.nextSibling;
          continue;
        }
        if (node.name == "key") {
          key = node.textContent;
          node = node.nextSibling;
          while (node.nodeType != Node.ELEMENT_NODE) {
            node = node.nextSibling;
          }
          attributes[key] = node.textContent;
        }
        node = node.nextSibling;
      }
      if (attributes['entropy'] == null
          || attributes['salt'] == null
          || attributes['iterations'] == null) {
        throw new Exception("incomplete hash data");
      }

      return attributes;
    }
  }
  
  throw new Exception("no hash found");
}

def writeHash(def username, def attributes, def writer) {
  def rounds = attributes['iterations'];
  def salt = base64ToHex(attributes['salt']);
  def hash = base64ToHex(attributes['entropy']);
  writer.println("${username}:\$ml\$${rounds}\$${salt}\$${hash}");
}

def base64ToHex(def base64) {
  byte[] decoded = base64.decodeBase64();
  return decoded.encodeHex();
}

def parseUserName(def plist) {
  def name = plist.getName();
  return name.substring(0, name.indexOf('.'));
}

def extractHashData(def plist) {
  def hashResult = "defaults read $plist ShadowHashData".execute();
  hashResult.waitFor();
  if (hashResult.exitValue() != 0) return null;
  def text = hashResult.text;
  text = text.substring(text.indexOf('<') + 1);
  text = text.substring(0, text.indexOf('>'));
  text = text.replaceAll("\\s+", "");
  return text;
}

def convertHashDataToXml(def hashData) {
  def file = shadowHashFile(hashData);
  try {
    convertResult = "plutil -convert xml1 ${file} -o -".execute();
    convertResult.waitFor();
    def text = convertResult.text.replaceAll("[\t\n\r]", "");
    def dbf = DocumentBuilderFactory.newInstance();
    def document = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(text)));
    return document;    
  }
  finally {
    secureRemove(file);
  }
}

def shadowHashFile(def hashData) {
  File file = File.createTempFile("hash", ".plist");
  def outputStream = new FileOutputStream(file);
  for (int i = 0; i < hashData.length() / 2; i++) {
    int b = Integer.parseInt(hashData.substring(2*i, 2*i + 2), 16);
    outputStream.write(b);
  }
  outputStream.close();
  return file;
}

def secureRemove(def path) {
  def result = "srm -rf ${path}".execute();
  result.waitFor();
  if (result.exitValue() != 0) {
    throw new Exception("failed to securely remove ${path}");
  }
}

class Gpg {

  final def gpgHome;

  Gpg() {
    def perms = PosixFilePermissions.fromString("rwx------");
    this.gpgHome = Files.createTempDirectory("gnupg-root", 
      PosixFilePermissions.asFileAttribute(perms)).toFile();
  }

  def importKey(def key) {
    File keyFile = File.createTempFile("itso-key", ".asc");
    try {
      FileWriter writer = new FileWriter(keyFile);
      writer.write(key);
      writer.close();
      run("--import ${keyFile}");    
    }
    finally {
      keyFile.delete();
    }
  }

  def encrypt(def source, def target, def keyId) {
    run("--batch --yes --trusted-key ${keyId} --output ${target} --armor --recipient ${keyId} --encrypt ${source}");
    Files.setPosixFilePermissions(target.toPath(), PosixFilePermissions.fromString("r--r--r--"));
  }

  def run(def subcommand) {
    def result = "gpg2 --yes --homedir ${gpgHome} ${subcommand}".execute();
    result.waitFor();
    if (result.exitValue() != 0) {
      System.err.print(result.err.text);
      throw new Exception("gpg command failed: ${subcommand}")
    }
  }

}

def itsoPublicKey() {
  return """-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: GPGTools - http://gpgtools.org

mQINBFIBJMQBEADnHJ3N/7Cn9nNx4PKdi/6q1G4fTReHIiv58uzUely5OY3yspCP
r3w+umIK6XulVrktMTopXY4jpMQpF++f+aOIPKaIZ+2nypHMiBlHibBzp+jBaTID
BIszsNHOwxD2aUZQXfXMJU/Z00QcU5/NkQjqvUe41QngfX9iHx8GyKi8TdPTB4YG
eCGYagSvfL2ZS4zUpvHcMJOXzxfzdK0zaLLD8B/jefo83rArqQeUyL1fgOe7imBj
OgUwyhsTdrKmf/yv/lkOYdSLAFDNbxNmOy3mQoOBl9ZM4SQtCwvWoNfbSXcOeD7y
hyvXUXjBYOLpHomIf2fheQODPqQvmCJLcFkPovx24723/iPHpzHkuQAIvQQIhKOq
RXxWHMlsPZVlrlbOiTllxTVlB5KIFtYHHLrAwtf+UnxG7+kGtJwMiXJQk0BUQxMW
xAl09z7KzB29UtVif999Bbyxdq2zPRXJsrrWnkT3lyeuoh+pWcIiA0/pYAjJu5Ma
o6A6sgZzZRLY7VKcR8zkqKAH3UdV6NDqFQ5TI6ODzFyVa44rsxmGYWs7gepL6ZSd
AA90Bl6A5PS+CdVf9UBqtUXPQeGQArbf778Kyg960zsRSk7k74+aVe4U50kPM1Lq
gS1x9Qt/uVI4ROgYUF5RX9HlODIluH9KNpD8PXK9CMoPdRQ3ow7sHsq6QQARAQAB
tBxWVCBJVCBTZWN1cml0eSA8aXRzb0B2dC5lZHU+iQI+BBMBCAAoBQJSASTEAhsD
BQkJZgGABgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIXgAAKCRByecdqD6xkEyCFEADi
AdkJncGDDo/uI1ZM9yVNrGdeC02Ab/dDua7TBuWD+YOskcZuKmGf+5ZOevzduRpT
S8tXY/QG5AOefQGtuAhzkKr1+i0NRJGtH/plTcJMN8VzutZ/u5ZRE4LfbkEz5lzW
V92RsvWwmpikR3M1yupPs6Qfy55Tyd9NW4eXqjfCUDW0kchaByAfNA5o5EsgOVnO
GnpPpxwaog/k9lCpax04rBCsUcJHeKvAOnyZpvOZItGQ8mttRLlVKyuecvblvsKf
zI0jyx0lVGeRGbuLYd4sDYX3JXKyjwvnjP76cvv2aVFSNIKos1wrHdocj+FX9Kiq
J33qyyXhQ0SeYhhueAwMO4XI3mcDIx0yH46RYn5HQO7TokdLgtBIWbZAUv3bQFB/
MVap39IByXnhmVWuPdsEF3zTfiAh9ElFpyBnIxnQAfTNzG6jJBT8TGcccga2VaL7
JqM4D6YVjW/rXCbsqc5CM68xbu+x7m3+vmAulm0mO/Yn4zyJnNiKVvV2XFLhUPbT
o5U8uaMgQchCaSrymqAOx4kXcxR3YCevxsLG41Em/Rs1KtFaMENYAJx15uW0l9yV
Ryx1m0anHG9fFCKjq7prhfcVqLMA86qXKnQK8Z5mA+hyNINDqeD65BpjuVz3Z4U4
m35Hn68sCxoXyKvzV6zv70nsMFMTdy6SuXx/7f5g1okCHAQQAQgABgUCUgE1+QAK
CRATo/3RZ7BXBX42EADV+sC9Nz27rQmSjRqbZVmi1GnggtNs1BncAaOsfc5C9gbm
5slYw5wO3S1XJrz/orqil4QiXjyIovAgloRU0xiJyw8lc215xUuyqZsqQW4U/o28
OT2TQ5A5ySpXVcmjbz52FblBaZWe4USgAsLYCAJRNRmK/IfykpoFeOx9CsTNy9EQ
KsEQwegn0ATdZklewypWXhKNeWMc3maVkndRAIrnoPFAhiAjqTGxxPp2LBvOA9oc
PovlLtaWYj6C4Qftptvg4etT8Xfbd/4X0/oG7021mVj5LbEozdDPtvsKb1JT8zPe
5M5V/EuaW8gzNe7YQHU5pdzFjZtMFry+U2h1KcFftsGrFD/AzY5jNQbin3822pYx
l07LS3mgzNZgcJTV/QqTWOFNpr9beetpbA3Ff36xPKVHZu6oVeFEVjW+k9rBgj+5
0S9Lt0/iPFRtf0bvKAzAWlCcyJW5PBrh8Mz4zQcSOMPyFkKeOxBKg302jmdgJQj4
8deSsDd4EBn263vC+ln29aBJhHzv4yfUqPQHtK8x40QKuIADMdZnL6Tu/2/tHnon
vM97/STApgSQy9igLKfEAzGWfbJeC2OhsMiSS76ByjsrOKkWm6BWEQzKZEje12uf
og+5Sn18pdPEDm36LhXZEeb7sUMJDlk/7+nIUDPZExkmvuLvIQXd2ODPFqVHTYkB
HAQTAQgABgUCUgFPZgAKCRDJKAkbkNgI4iXTB/0WxRkmW5b6JZ8USqmuxo4ZEXkf
lG+//FAJXJ7UdEOtpW82ClD/oomMdPjifgC7F9zkgtwb5VtK5OwZTQCT8V5jBlWD
AWNJh4gJ4zAMeEzq6TNRw3JJ5Q2daHEBiLKztjykdKH6w0RvToX7Dw+eNy1TxHa7
O9QRl2n5hPlxyRoIr+Kts3/IBs0ZZUVjW+pbSihbm2mNAZ054q71nbvm9zcMuxs4
P16+PoNpL5+8j5G2q4GyjhWKAkUj5udPzuNsTKsXzDocrJW/yrJf3ZhV34WlZ89b
WKtdzZVXYcr6NxkxZqd7PwiWI79JrEAbK3cx28FQhF+2vcCXiKSagxMdzpWeiJwE
EwEIAAYFAlICQbYACgkQf9uwjdgnWD3SOAP/djFh+aJxTAAAcsRKgfCppFryIA0n
gGVreYPaB6muYh/M6dKJSu8C85TlY3V4AyOFV8JHIpbxEK8RKLqQoUX7q4mnVsX2
VhZhJjPI7fFHoPnW+9WdbW6CM/w6GCB2Jfyb/MO/POwqKiftdGSuDw+n1dnYHnHv
dyQAetFHWgjOqA+JARwEEwECAAYFAlIDiVUACgkQNMlrxYAxn5TLJwf9EYIBn95G
N4i8+bBUJnH08zDb/9bX9hLNK5vJQbZVEFzfD7M4cXMvBk/wfdgcY7amWhIYGKVN
i0deo20dj32nJJo8CLqy+VQqCVOSW/0J/t9WAdr2+LdVRUeNtc8Lo1wRDReg6RGq
TXs0XHZimjt1KDOqTZxZHf6W1dqRwqJKz4mqbDeOyjjCoiCmrkB1KI2yPV1dr8/E
xRXYgVD2OMIyJMAF8H+SPzkSnhF9885MNQ3p9pElvZBk6Fl+pOUT6H2vQ2hbWmCo
PlyE7hWtGD8yGJ07veQLnKvrEllwhb+pavdpXPnw3B2d9OrJSOkdRE2CZsreTXgH
RsV4QsNpA6W9yYkBnAQTAQgABgUCUgJBgAAKCRCDN4qU+mxJlPoKC/9nDqyastyr
s/l93B4J4oSkEwKjOLNgNV9WJ1OwgQQc9Cf3sOlS/iv+x3T7bGyhpOws8fFzxjsH
0l4SPHPkTsF/QU2VYukS6IGPyjdH1oBjkJIVwPW5De18uSmUHXdDmlEnBRWEnueA
kmi0uLCLWp5TCG/CCoGJdT7H6RP2H4vVTEmpuVFuhPQXcsKeCQjwqW0ReaUGUOmY
++YGFnCKkW9FOSQA8fvnus00GzmwFLKpLDneFWX9Qu0psC0ZGX8jJsrocp+AfLVF
D+S87QybMi3fbakp6rpjex1CueFaOfl7kDRTuY/Xw2UETHL9FOFs5aIUZSNyHwaf
SKhRonfYKYMEcqfKNmHyWtY0h7AGGSSzBjhvwOfa1xNAfsiSWZoMr4346K32u0NU
4m/ZQ9U5Lps5IW6vVCbgasiaGHlikQWKL0gpuuAqKRafc8Jqp8eplYviporhMPs6
xhzVrg4Z7ZZJXxiznJXgbDgc0jfwt9VPbjHQS9ziGxlzb1HFE+/0EmuJAhsEEwEI
AAYFAlICTTsACgkQE+Dz3m702pJb6Q/47DyBf1Qk3z6NqYE9AbeWZDPY86B3ybuC
xpdiJqQbjpisUe4tmjR+Lm3Vc2DHULmiX2sx/idu0WMiiKkTA6JBQ3eEwKd2vflx
feTQJwXQ0ZHsmW29chXXItxmwhqqDgQ4LGHRR3C0FtycKIKL3AIEq3m+BEGHHr7D
V9Cc/AEQIdxajnsOTyfIonY7kvWW8Glpv/RiAAdyiaA2vUx0q9iHyiYN4GaxPCVM
x7VVisdjMQtN+dUy6nhGWjTzdkOgD/B9SxciTwt/k89dsrvPXcatcehKx1mwLA1t
AK7cBBoTKNED910eJXJiq4cUk5KmO/hBCN1T3o97oiJ/RuqtWTtaf/8pURBdZ5oE
mJ+90ldA9iVe9S+yzTfXxApzMXlRqeZmlSE5+R6dAoo6Vm6sCla9N/Q8TSWjHc5G
EPnmFu7ExFu9U1catFySGXJjxdj7RluxqWKG8R6y47RPZ63OGFonYM/mjQTefJsv
+qo0uK4PODVDVlDvlhKoxw1VKVsmeeiGeSKcoDQSaSsddT1ugp95DDRVMccLVcHJ
lHiArhnMbojYjxlyLTpkGAGfKfJAGj8Y6sZNvz7ccGgu6sF4Sl/cOVuELEkiJ9oZ
YvrEsv6slm/lAVecW21Ick1ElP87hKUbyWXeb/LmoDGSrK7EUrbBQXRf3zfri6qr
/PQn8Fga7okCHAQQAQIABgUCUgFOPwAKCRDLBZWm2DyHx2bfEACA2EJJbsEFf3+R
8JHCr2IqF2MC06vRkB3NW0a51Zz/obeT1s1VfkGxXl6Ao6Ym5KDE5/b38s8R8efB
IWMzKfKntdgKaAClOD9FNSq7vLb9IIU9X0/4NGLiC8hatRTl6zsbUnWApdPESZp7
E9KUPwT341KPu8pYvZkLx/SahPXqrsbk18arGkfKbSBshtZamehJq8aHx4k9QJBe
HYG3FywbpaKqkujXw5W+tdtNMxMDDMxSVxrbjqhn9YDAJshGC+dI/3kvLx2jI7aq
hfTA+IwJBjsymn6Hhp+Cfj55VhmLC3gCozqCguWjrvXnSL6TUzRMR7P4j9/yEjZ5
sTXV4ZxnZ6FRQpc42YmObe+Hq97+i7XCJfKCEuA7iPaERT+d9VembSAgth5cJBLn
CvdUYr/wOqJpeaGwdG/QffFud4WpJbRJH2TCGTfJdw2YLP382xqmQXCT2bYmdM8Q
6J0/AcXa50iw1CTaqaObCL90bzOG8FlaO8PrTVJSAObGcNqaUG82eRSxeRRWzXdF
F019PT1bNpFn/S5Z/ymH/2VfsseuP3BCpRg6oFe0HVWGkUm7hMug+3s5JIgVau1p
bA1/TAqF6nrZ3QjphEjyryOzj6s3u+8DXP2h3qR4+JHw5rrdnwsYGuSAr+QcyKKO
C6rlqbDNaXTzJBpZL6uzo5e1jLjytokCHAQQAQIABgUCUgGr3QAKCRCjqyrsgJX1
fcU3EACV6teOWZm5zgra7H4qDNERouBtJwsQ78gMe2l9PjrFUSlrFnszQeXC59si
l2QOGcce0jZ8TjI/eTL2eggV7vGHe7DxwH6oaPPis0WtyqoOFnNzdVbPUtTFZUZF
/TzQUO+ynRJSsjQrL4ibZaXXJcwwspyzD3j1H9BBRNYhz1Fm0ghHLCcVo4K/PSwX
g/i/XyeVUm6N+RmixfPu1kbQbLFBVx/lUbo7DNTZYCKS08dciUszw8nbOuujeV0X
JRSll1IEAfCpDrqWQmFo5Hyfsaf2dG2LXbTaBCVmBg7bfmGQfO3DMGC9mqL6ZIVl
q6W+ShzFk5eY1TsTm8lNdk2sQndYPUxm2NEbIbV7iU40F2wLKODHcZTHgEkitT7G
mnyaW4O1YWNMHlozKTgzGh1iTZ/KpZJs2alU0mZB4Id2UoGbLtdwoQrdkddNI6VQ
MsqYqRw9u5FelFiCYqD3doLOtBC6GjXd3djFMlLRYnVE4v6mro7Q+zGo/CwcyTH+
/bCXm1MgGuWpK2Aj22/fOIFHFfVfB1Qn0ApkiY254/LedoJPvcYZSljIpZMlmjBX
UJjD5JFNOYoj3xJ/7FGZjSQ6HdXnlQhS4AxgQP9gAT8x1ysjTYi7NwwcXGqY4Bfd
cd9FLof7LWdgcGidNTNIKRh2YFEgkvIXQBvyPDa3MH8gY+meWokCHAQQAQgABgUC
UgFRNwAKCRAHZhEFg10ToK9OD/0X5xsiy1LaVEgKTCMjQeY3S6hD0MflVJIIL9jH
DbcRu/EwW9UL9XFL1iL9nWvJgr7i+42N0Y16I9+laoT0caKDRYxtaNZwaxmyyP4V
WCh8O32JOd+mHuzd3IioMR9yd0D3CTwUphV8yoPXJEJ1/0TwXn2EmnLNfDrpqJ/a
V70tQPwmSQ2jexxfwSN8hwkAU+ha6JsHF3mI4YenFW1QA1tDnSEJvHl+EEAS8VzX
7gWGN3Y2YF04Mi571eSmaiwrKKES+oAF2Fq3Of1uSMy17t7ftXsRfGNzCUT72Ov/
fzTTHhO4arVMyIuUQFSdLKC0CS5LvEjJfNyVdXh/Q371OhIr5D9IDQyGaSx66Aj7
OoDSSPzv4X73rc98D1PfDEncn9Sl+obxvSO3WFUvdoJ76SV4MAdw9WyVyvusHchc
+wuZrbu/HhlBQB43ObW3931e6MS/qXWwoaaxzlDcaS1YKkoFR8HEylurdME3HV2O
y+4iPQ72ZB5LamAvxugeYmR0EoZc+SnhVqYpy4EAplAQtWL3B9189JQ5wJ/Vogzz
04bczaDvqVT7/SuZD37b9FmIsBmER1t53THuwaQsD5BS1Dq8irczjr4Rd9Aw+p8D
UpEoKHwy87lgNzjwU6EA5Hpmxw38nBLZyENdp64FCe5K3cyGOw4p4xpz1yR4avLt
kfBq3LkCDQRSASTEARAAsbwGjjFfGO6r9Hso1KqSovQCkdLQLNXK9kZfoDuDXdbG
6fsR17zogAjgtSGPvYASZ4omCG4nteeF8/DpCp2QCl5n6xLKLSkFqpM1ju+NftNO
dhfzTsyDZw6MQA/rLz/BZh1pXNCRx3RqaCVKisMnotPKLGUI/h5Jd286aLGIvImH
ZWMarEK5HfLdnMVYV1yk5je8MYVI8USrqoHaKY/RjI8AKNvl49LG/X7uFsFmYIRc
3e5387wDpOQYtjiqOEzOTewh0bZfUG8z4Ose3t/Wou2fN12rOz7IbH6l+8kr1HMe
LFSYChDBm3MNcX0t0ObqufmdT75G2Vl8TzDs0Fb6FrBAwzZqmRidlB0+FW0TYW4I
rOGg/ZZuqOOJhqDQT9pWkXVeJlcGuY+hBXpeClxHfFFNqcajVw/04EFQZUUIED6S
Zn64TYIt4Wm+qHudjhl1vk2rO1ItP0+g7mbgeJNjPtAJDFMzUQDgidkRqv7QSm3Q
XCXOgQPiq/w5DR9E0vlZdInOAZP3U6x/KaBD7UOJUKTYejnGVP6aMp7Tn7hVcL8h
5vbCKPRLVknSfdtlTCWo303uBhvj0I757Rje76EEbJDbAPfnJHnH8Zc1X3BHy3eO
IPUVSwv9qKu6UjjYPn2J2AqwNqhH3qMmZJcJsjNXqcFKKJx6v8zQ58NYTak8zvMA
EQEAAYkCJQQYAQgADwUCUgEkxAIbDAUJCWYBgAAKCRByecdqD6xkE1NrEACJGLSR
KvZpS4ega+4waOsaJKN4CSYQ7Mz4VOqVK6oPpTgXdy/8+DhNgQM22Iq1zGyVLS+7
oMGnTcgmm/bkHU96kjvni3AluS8t0ttcYUduvcFEmIYoR26oTkig+htpWxvnkEOI
dWOGN6RbHfETdJbtJ6TErHFrUemJbf+Bd7Yh+goc4tjEI5i0n7oESsZg+E3Q4eWU
H1rti0az/PDEMNVt/Krc38sdxK2UjHM0NNlpIY5PFYqGuWjvi2FADPI/j6ZqjnlH
GCpphZR0HwhqJfyu4BakrN706JyAS4LqglkGe3FGYvX3q6T1YtlRybp3xXezvzjb
LEhCWV12zumNJT9ZkKtm3s0JBc7ef0yxOS0aZAxmlDsx6/OO59RVDKQdOv5xWezp
niiE+mPv0htnxgags5T4zWUox1gJqcB6rvDRrK46s1HutMoYf7R5jKvvkQj1crp7
Ct/MNPoef+yCNlA+mEAf0CsVNgrtBhJAZhYs2yqR2kK8PXxnve3GQ+rJnfFec1AG
APzGMAxLF6UXBaWCRCBfh87inOrIvrsGTCB3BrkPq+e7BNNKMjb41zlpCusnFrZp
8es1w7CpOcNt/hoJtfPluX/AN38eQm7U9gxRp0hpDz/7MmtcTQl2JJ8/A2anaIsU
k8gGDrHy/2WnKEuK+UO5fJk8qcmsPxGtLNjWFw==
=BTe6
-----END PGP PUBLIC KEY BLOCK-----
""";
}
